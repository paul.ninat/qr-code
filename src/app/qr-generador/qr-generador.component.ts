import { Component, ViewChild, ElementRef } from '@angular/core';
import * as QRCode from 'qrcode';
import { FormsModule } from '@angular/forms';
import { EnviarQrService } from '../enviar-qr.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
//import { saveAs } from 'file-saver';

@Component({
  selector: 'app-qr-generador',
  templateUrl: './qr-generador.component.html',
  styleUrls: ['./qr-generador.component.css']
})
export class QrGeneradorComponent {
  private readonly API_URL = 'http://localhost:4200'; // Cambiar por la URL de tu API

  qrTexto: string = '';
  pdfSrc: string = '';

  @ViewChild('qrCode') qrCode!: ElementRef;

  texto1!: string;
  texto2!: string;
  fecha!: Date;
  constructor(private enviarQrService: EnviarQrService, private http: HttpClient ) {}
  
  generarQR(): void {
    const textoQR = `nombre: ${this.texto1} \n Apellido: ${this.texto2} \n Fecha: ${this.fecha.toString()}`;
    QRCode.toDataURL(textoQR, (err, url) => {
      if (err) {
        console.error(err);
      } else {
        this.qrCode.nativeElement.src = url;
        this.enviarQR(url);
      }
    });

    /* let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    let options = {
      headers: headers,
      responseType: 'arraybuffer' as 'json'
    };
    let body = {
      qrTexto: this.qrTexto
    };

    // Petición HTTP para generar el reporte
    this.http.post('http://localhost:4200/jasper_report/generarqr', body, options)
      .subscribe(
        (data) => {
          // Si la petición es exitosa, muestra el PDF
          let blob = new Blob([data], { type: 'application/pdf' });
          this.pdfSrc = URL.createObjectURL(blob);
          saveAs(blob, 'qr_report.pdf');
        },
        (error) => {
          // Si hay un error, muestra un mensaje de error
          console.error(error);
        }
      ); */
  }
      
  

  enviarQR(imageData: string): void {
    this.enviarQrService.enviarQR(imageData)
      .then(response => {
        console.log('Imagen QR enviada al archivo Jasper Report');
      })
      .catch(error => {
        console.error('Error al enviar la imagen QR al archivo Jasper Report:', error);
      });
  }

  /* generarReporteQR(): void {
    const options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'blob' };
    this.http.post(`${this.API_URL}/jasper_report/QRGenerator.jrxml`, { }, options)
      .subscribe((response: Blob) => {
        const file = new Blob([response], { type: 'application/pdf' });
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      }, error => console.error(error));
  } */

}