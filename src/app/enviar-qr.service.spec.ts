import { TestBed } from '@angular/core/testing';

import { EnviarQrService } from './enviar-qr.service';

describe('EnviarQrService', () => {
  let service: EnviarQrService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnviarQrService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
