import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EnviarQrService {
  private apiUrl = './jasper_report/QRGenerator.jrxml';
  
  
  constructor(private http: HttpClient) { }

  enviarQR(imageData: string): Promise<any> {
    console.log("imagen : "+imageData)
    const payload = { qrImage: imageData };
    console.log("qrimage: "+payload)
    console.log("apiurl:",this.apiUrl)
    return this.http.post(this.apiUrl, payload).toPromise();
  } 
}
